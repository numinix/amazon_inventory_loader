# Amazon Inventory Loader

SET @configuration_group_id=0;
SELECT @configuration_group_id:=configuration_group_id
FROM configuration_group
WHERE configuration_group_title= 'Amazon Inventory Loader Configuration'
LIMIT 1;
DELETE FROM configuration WHERE configuration_group_id = @configuration_group_id AND configuration_group_id != 0;
DELETE FROM configuration_group WHERE configuration_group_id = @configuration_group_id AND configuration_group_id != 0;

INSERT INTO configuration_group (configuration_group_id, configuration_group_title, configuration_group_description, sort_order, visible) VALUES (NULL, 'Amazon Inventory Loader Configuration', 'Set Amazon Inventory Loader Options', '1', '1');
SET @configuration_group_id=last_insert_id();
UPDATE configuration_group SET sort_order = @configuration_group_id WHERE configuration_group_id = @configuration_group_id;

INSERT INTO configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added, use_function, set_function) VALUES 
(NULL, 'Numinix Product Fields', 'AMAZON_IL_ATTRIBUTES', 'false', 'Is Numinix Product Fields Installed?', @configuration_group_id, 0, NOW(), NULL, 'zen_cfg_select_option(array(\'true\', \'false\'),'),
(NULL, 'Output File Name', 'AMAZON_IL_OUTPUT_FILENAME', 'MyAmazonInventory.csv', 'Set the name of your Amazon Inventory Loader output file', @configuration_group_id, 0, NOW(), NULL, NULL),
(NULL, 'Output Directory', 'AMAZON_IL_DIRECTORY', 'feed/', 'Set the name of your Amazon Inventory Loader output directory', @configuration_group_id, 0, NOW(), NULL, NULL),
(NULL, 'Compress Feed File', 'AMAZON_IL_COMPRESS', 'false', 'Compress Amazon file?', @configuration_group_id, 0, NOW(), NULL, 'zen_cfg_select_option(array(\'true\', \'false\'),'),

(NULL, 'Included Categories', 'AMAZON_IL_POS_CATEGORIES', '', 'Enter category ids separated by commas <br>(i.e. 1,2,3)<br />Leave blank to allow all categories', @configuration_group_id, 10, NOW(), NULL, NULL),
(NULL, 'Excluded Categories', 'AMAZON_IL_NEG_CATEGORIES', '', 'Enter category ids separated by commas <br>(i.e. 1,2,3)<br />Leave blank to deactivate', @configuration_group_id, 10, NOW(), NULL, NULL),
(NULL, 'Excluded Keywords', 'AMAZON_IL_NEG_KEYWORDS', 'oem', 'Enter title keywords separated by commas <br> (i.e. oem,gift,free,)<br />Leave blank to deactivate', @configuration_group_id, 10, NOW(), NULL, NULL),
(NULL, 'Max products', 'AMAZON_IL_MAX_PRODUCTS', '0', 'Default = 0 for infinite # of products', @configuration_group_id, 11, NOW(), NULL, NULL),
(NULL, 'Min Price', 'AMAZON_IL_MIN_PRICE', '0', 'Default = 0 for no minimum', @configuration_group_id, 12, NOW(), NULL, NULL),
(NULL, 'Max Price', 'AMAZON_IL_MAX_PRICE', '0', 'Default = 0 for no maximum', @configuration_group_id, 12, NOW(), NULL, NULL),

(NULL, 'Condition', 'AMAZON_IL_CONDITION', 'NEW', 'Enter condition', @configuration_group_id, 20, NOW(), NULL, NULL),
(NULL, 'UPC', 'AMAZON_IL_UPC', 'false', 'Use UPC as StandardProductID?', @configuration_group_id, 21, NOW(), NULL, 'zen_cfg_select_option(array(\'true\', \'false\'),'),
(NULL, 'ISBN', 'AMAZON_IL_ISBN', 'false', 'Use ISBN as StandardProductID?', @configuration_group_id, 21, NOW(), NULL, 'zen_cfg_select_option(array(\'true\', \'false\'),'),
(NULL, 'Default Quantity', 'AMAZON_IL_DEFAULT_QUANTITY', '10', 'Enter default quantity for 0 quantity items:', @configuration_group_id, 22, NOW(), NULL, NULL), 

(NULL, 'International Shipping', 'AMAZON_IL_INTERNATIONAL', 'n', 'Will you ship internationally?', @configuration_group_id, 30, NOW(), NULL, 'zen_cfg_select_option(array(\'y\', \'n\'),'),
(NULL, 'Expedited Shipping', 'AMAZON_IL_EXPEDITED', 'n', 'Will you ship expedited?', @configuration_group_id, 30, NOW(), NULL, 'zen_cfg_select_option(array(\'y\', \'n\'),'),
(NULL, 'Amazon Marketplace', 'AMAZON_IL_MARKETPLACE', 'n', 'Appear in Amazon Marketplace listings?', @configuration_group_id, 30, NOW(), NULL, 'zen_cfg_select_option(array(\'y\', \'n\'),');

# the following may return warnings if they already exist.  these warnings can be ignored
ALTER TABLE products ADD products_condition varchar(32) NULL default NULL;
ALTER TABLE products ADD products_upc varchar(32) NULL default NULL;
ALTER TABLE products ADD products_isbn varchar(32) NULL default NULL;
ALTER TABLE products ADD products_amazon varchar(32) NULL default NULL;