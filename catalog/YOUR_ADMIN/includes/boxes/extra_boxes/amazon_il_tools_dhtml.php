<?php
/**
 * amazon_il.php
 *
 * @package Amazon Inventory Loader
 * @copyright Copyright 2007-2008 Numinix Technology http://www.numinix.com
 * @copyright Portions Copyright 2003-2006 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: amazon_il_tools_dhtml.php, v0.99 02.15.2007 12:58:59 numinix $
 */
$za_contents[] = array('text' => BOX_AMAZON_IL, 'link' => zen_href_link(FILENAME_AMAZON_IL, '', 'NONSSL'));
?>