Install:
0. Install Numinix Product Fields with the UPC/ISBN fields.
1. Add UPC and/or ISBN codes to all of your products. Products without codes cannot be loaded into Amazon with this tool.
2. Upload all files to your server maintaining the directory format.
3. Copy and paste the contents of the install.sql file into ADMIN->TOOLS->INSTALL SQL PATCHES.
4. Go to ADMIN->CONFIGURATION->AMAZON INVENTORY LOADER CONFIGURATION and set all parameters.
5. Go to ADMIN->TOOLS->AMAZON INVENTORY LOADER and run the script to create your Amazon Inventory Loader CSV file.
6. Open the CSV file in Microsoft Excel and Save As TABBED (.TXT) format.
7. Import into your account at Amazon.com.

Uninstall:
1. Delete all included files.
2. Copy and paste the contents of the uninstall.sql file into ADMIN->TOOLS->INSTALL SQL PATCHES.