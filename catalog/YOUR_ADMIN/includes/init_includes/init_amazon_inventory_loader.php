<?php

if (!defined('IS_ADMIN_FLAG')) {
    die('Illegal Access');
}

$module_constant = 'AMAZON_IL_VERSION';
$module_installer_directory = DIR_FS_ADMIN . 'includes/installers/amazon_inventory_loader';
$module_name = "Amazon Inventory Loader";
$zencart_com_plugin_id = 0; // from zencart.com plugins - Leave Zero not to check
//Just change the stuff above... Nothing down here should need to change

$configuration_group_id = '';
if (defined($module_constant)) {
    $current_version = constant($module_constant);return;
} else {
    $current_version = "0.0.0";
    $db->Execute("INSERT INTO " . TABLE_CONFIGURATION_GROUP . " 
    (configuration_group_title, configuration_group_description, sort_order, visible) 
    VALUES 
    ('" . $module_name . "', 'Set " . $module_name . " Options', '1', '1');");

    $configuration_group_id = $db->Insert_ID();

    $db->Execute("UPDATE " . TABLE_CONFIGURATION_GROUP . " SET sort_order = " . $configuration_group_id . " WHERE configuration_group_id = " . $configuration_group_id . ";");

    $db->Execute("INSERT INTO " . TABLE_CONFIGURATION . " 
    (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) 
    VALUES ('Version', '" . $module_constant . "', '0.0.0', 'Version installed:', " . $configuration_group_id . ", 0, NOW(), NOW(), NULL, NULL);");

    $db->Execute("INSERT INTO configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added, use_function, set_function) VALUES 
                (NULL, 'Numinix Product Fields', 'AMAZON_IL_ATTRIBUTES', 'false', 'Is Numinix Product Fields Installed?', '$configuration_group_id', 0, NOW(), NULL, 'zen_cfg_select_option(array(\'true\', \'false\'),'),
                (NULL, 'Output File Name', 'AMAZON_IL_OUTPUT_FILENAME', 'MyAmazonInventory.csv', 'Set the name of your Amazon Inventory Loader output file', '$configuration_group_id', 0, NOW(), NULL, NULL),
                (NULL, 'Output Directory', 'AMAZON_IL_DIRECTORY', 'feed/', 'Set the name of your Amazon Inventory Loader output directory', '$configuration_group_id', 0, NOW(), NULL, NULL),
                (NULL, 'Compress Feed File', 'AMAZON_IL_COMPRESS', 'false', 'Compress Amazon file?', '$configuration_group_id', 0, NOW(), NULL, 'zen_cfg_select_option(array(\'true\', \'false\'),'),
                
                (NULL, 'Included Categories', 'AMAZON_IL_POS_CATEGORIES', '', 'Enter category ids separated by commas <br>(i.e. 1,2,3)<br />Leave blank to allow all categories', '$configuration_group_id', 10, NOW(), NULL, NULL),
                (NULL, 'Excluded Categories', 'AMAZON_IL_NEG_CATEGORIES', '', 'Enter category ids separated by commas <br>(i.e. 1,2,3)<br />Leave blank to deactivate', '$configuration_group_id', 10, NOW(), NULL, NULL),
                (NULL, 'Excluded Keywords', 'AMAZON_IL_NEG_KEYWORDS', 'oem', 'Enter title keywords separated by commas <br> (i.e. oem,gift,free,)<br />Leave blank to deactivate', '$configuration_group_id', 10, NOW(), NULL, NULL),
                (NULL, 'Max products', 'AMAZON_IL_MAX_PRODUCTS', '0', 'Default = 0 for infinite # of products', '$configuration_group_id', 11, NOW(), NULL, NULL),
                (NULL, 'Min Price', 'AMAZON_IL_MIN_PRICE', '0', 'Default = 0 for no minimum', '$configuration_group_id', 12, NOW(), NULL, NULL),
                (NULL, 'Max Price', 'AMAZON_IL_MAX_PRICE', '0', 'Default = 0 for no maximum', '$configuration_group_id', 12, NOW(), NULL, NULL),
                
                (NULL, 'Condition', 'AMAZON_IL_CONDITION', 'NEW', 'Enter condition', '$configuration_group_id', 20, NOW(), NULL, NULL),
                (NULL, 'UPC', 'AMAZON_IL_UPC', 'false', 'Use UPC as StandardProductID?', '$configuration_group_id', 21, NOW(), NULL, 'zen_cfg_select_option(array(\'true\', \'false\'),'),
                (NULL, 'ISBN', 'AMAZON_IL_ISBN', 'false', 'Use ISBN as StandardProductID?', '$configuration_group_id', 21, NOW(), NULL, 'zen_cfg_select_option(array(\'true\', \'false\'),'),
                (NULL, 'Default Quantity', 'AMAZON_IL_DEFAULT_QUANTITY', '10', 'Enter default quantity for 0 quantity items:', '$configuration_group_id', 22, NOW(), NULL, NULL), 
                
                (NULL, 'International Shipping', 'AMAZON_IL_INTERNATIONAL', 'n', 'Will you ship internationally?', '$configuration_group_id', 30, NOW(), NULL, 'zen_cfg_select_option(array(\'y\', \'n\'),'),
                (NULL, 'Expedited Shipping', 'AMAZON_IL_EXPEDITED', 'n', 'Will you ship expedited?', '$configuration_group_id', 30, NOW(), NULL, 'zen_cfg_select_option(array(\'y\', \'n\'),'),
                (NULL, 'Amazon Marketplace', 'AMAZON_IL_MARKETPLACE', 'n', 'Appear in Amazon Marketplace listings?', '$configuration_group_id', 30, NOW(), NULL, 'zen_cfg_select_option(array(\'y\', \'n\'),')");

global $sniffer;
if (!$sniffer->field_exists(TABLE_PRODUCTS, 'products_condition')) $db->Execute("ALTER TABLE products ADD products_condition varchar(32) NULL default NULL;");

if (!$sniffer->field_exists(TABLE_PRODUCTS, 'products_upc')) $db->Execute("ALTER TABLE products ADD products_upc varchar(32) NULL default NULL;");
if (!$sniffer->field_exists(TABLE_PRODUCTS, 'products_isbn')) $db->Execute("ALTER TABLE products ADD products_isbn varchar(32) NULL default NULL;");
if (!$sniffer->field_exists(TABLE_PRODUCTS, 'products_amazon')) $db->Execute("ALTER TABLE products ADD products_amazon varchar(32) NULL default NULL;");

}
if ($configuration_group_id == '') {
    $config = $db->Execute("SELECT configuration_group_id FROM " . TABLE_CONFIGURATION . " WHERE configuration_key= '" . $module_constant . "'");
    $configuration_group_id = $config->fields['configuration_group_id'];
}


$installers = scandir($module_installer_directory, 1);

$newest_version = $installers[0];
$newest_version = substr($newest_version, 0, -4);

sort($installers);
if (version_compare($newest_version, $current_version) > 0) {
    foreach ($installers as $installer) {
        if (version_compare($newest_version, substr($installer, 0, -4)) >= 0 && version_compare($current_version, substr($installer, 0, -4)) < 0) {
            include($module_installer_directory . '/' . $installer);
            $current_version = str_replace("_", ".", substr($installer, 0, -4));
            $db->Execute("UPDATE " . TABLE_CONFIGURATION . " SET configuration_value = '" . $current_version . "' WHERE configuration_key = '" . $module_constant . "' LIMIT 1;");
            $messageStack->add("Installed " . $module_name . " v" . $current_version, 'success');
        }
    }
}



if (!function_exists('plugin_version_check_for_updates')) {
    function plugin_version_check_for_updates($fileid = 0, $version_string_to_check = '') {
        if ($fileid == 0){
            return FALSE;
        }
        $new_version_available = FALSE;
        $lookup_index = 0;
        $url = 'http://www.zen-cart.com/downloads.php?do=versioncheck' . '&id=' . (int) $fileid;
        $data = json_decode(file_get_contents($url), true);
        if (!$data || !is_array($data)) return false;
        // compare versions
        if (version_compare($data[$lookup_index]['latest_plugin_version'], $version_string_to_check) > 0) {
            $new_version_available = TRUE;
        }
        // check whether present ZC version is compatible with the latest available plugin version
        if (!in_array('v' . PROJECT_VERSION_MAJOR . '.' . PROJECT_VERSION_MINOR, $data[$lookup_index]['zcversions'])) {
            $new_version_available = FALSE;
        }
        if ($version_string_to_check == true) {
            return $data[$lookup_index];
        } else {
            return FALSE;
        }
    }
}

// Version Checking
if ($zencart_com_plugin_id != 0) {
    if ($_GET['gID'] == $configuration_group_id) {
        $new_version_details = plugin_version_check_for_updates($zencart_com_plugin_id, $current_version);
        if ($new_version_details != FALSE) {
            $messageStack->add("Version " . $new_version_details['latest_plugin_version'] . " of " . $new_version_details['title'] . ' is available at <a href="' . $new_version_details['link'] . '" target="_blank">[Details]</a>', 'caution');
        }
    }
}