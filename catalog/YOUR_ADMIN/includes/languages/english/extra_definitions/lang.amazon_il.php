<?php
/**
 * amazon_il.php
 *
 * @package Amazon Inventory Loader
 * @copyright Copyright 2007-2008 Numinix Technology http://www.numinix.com
 * @copyright Copyright 2003-2006 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: amazon_il.php, v0.99 02.15.2008 13:04:59 numinix $
 */

$define = [
    'TEXT_AMAZON_IL','Amazon Inventory Loader',
    'BOX_AMAZON_IL', 'Amazon Inventory Loader'
];

$zc158 = (PROJECT_VERSION_MAJOR > 1 || (PROJECT_VERSION_MAJOR == 1 && substr(PROJECT_VERSION_MINOR, 0, 3) >= '5.8'));
if ($zc158) {
    return $define;
} else {
    nmx_create_defines($define);
}
?>