<?php
/**
 * amazon_il.php
 *
 * @package Amazon Inventory Loader
 * @copyright Copyright 2007-2008 Numinix Technology http://www.numinix.com
 * @copyright Portions Copyright 2003-2006 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: amazon_il.php, v0.99 02.15.2008 13:02:59 numinix $
 */

$define = [
    'HEADING_TITLE' => 'Amazon Inventory Loader',
    'TEXT_AMAZON_IL_OVERVIEW_HEAD' => '<p><strong>OVERVIEW:</strong></p>',
    'TEXT_AMAZON_IL_OVERVIEW_TEXT' => '<p>This module automatically generates an Amazon Inventory Loader bulk file from your Zen Cart store.</p>',
    'TEXT_AMAZON_IL_INSTRUCTIONS_HEAD' => '<p><strong>INSTRUCTIONS: </strong></p>',
    'TEXT_AMAZON_IL_INSTRUCTIONS_STEP1' => '<p><strong><font color="#FF0000">STEP 1:</font></strong> Click <a href=%s><strong>[HERE]</strong></a> to create / update your product feed. </p>',
    'TEXT_AMAZON_IL_INSTRUCTIONS_STEP1_NOTE' => '<p>NOTE: You may <a href="' . HTTP_SERVER . DIR_WS_CATALOG . AMAZON_IL_DIRECTORY . AMAZON_IL_OUTPUT_FILENAME . '" target="_blank" class="splitPageLink"><strong>view</strong></a> your product feed before uploading to Amazon.com. </p>'
];

$zc158 = (PROJECT_VERSION_MAJOR > 1 || (PROJECT_VERSION_MAJOR == 1 && substr(PROJECT_VERSION_MINOR, 0, 3) >= '5.8'));
if ($zc158) {
    return $define;
} else {
    nmx_create_defines($define);
}
?>