<?php

// For Admin Pages
$zc150 = (PROJECT_VERSION_MAJOR > 1 || (PROJECT_VERSION_MAJOR == 1 && substr(PROJECT_VERSION_MINOR, 0, 3) >= 5));
if ($zc150) { // continue Zen Cart 1.5.0
  // delete configuration menu
  $db->Execute("DELETE FROM ".TABLE_ADMIN_PAGES." WHERE page_key = 'toolsAIL' LIMIT 1;");
  // add configuration menu
  if (!zen_page_key_exists('toolsAIL')) {
    if ((int)$configuration_group_id > 0) {
      zen_register_admin_page('toolsAIL',
                              'TEXT_AMAZON_IL', 
                              'FILENAME_AMAZON_IL',
                              '', 
                              'tools', 
                              'Y',
                              $configuration_group_id);
        
      $messageStack->add('Enabled Amazon Inventory Loader Tools Menu Item.', 'success');
    }
  }
}


if(version_compare(PROJECT_VERSION_MAJOR.".".PROJECT_VERSION_MINOR, "1.5.0") >= 0) { 
  // continue Zen Cart 1.5.0
    // add to configuration menus
  if (function_exists('zen_page_key_exists') && function_exists('zen_register_admin_page') && !zen_page_key_exists('configAIL')) {
    zen_register_admin_page('configAIL',
                            'TEXT_AMAZON_IL', 
                            'FILENAME_CONFIGURATION',
                            'gID='.(int)$configuration_group_id, 
                            'configuration', 
                            'Y',
                            999);
      
    $messageStack->add('AIL Configuration menu.', 'success');
  }
}