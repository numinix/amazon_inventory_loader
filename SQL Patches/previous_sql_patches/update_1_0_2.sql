SET @configuration_group_id=0;
SELECT @configuration_group_id:=configuration_group_id
FROM configuration_group
WHERE configuration_group_title= 'Amazon Inventory Loader Configuration'
LIMIT 1;

INSERT INTO configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added, use_function, set_function) VALUES 
(NULL, 'Default Quantity', 'AMAZON_IL_DEFAULT_QUANTITY', '10', 'Enter default quantity for 0 quantity items:', @configuration_group_id, 22, NOW(), NULL, NULL);

UPDATE configuration SET configuration_title = 'Numinix Product Fields', configuration_description = 'Is Numinix Product Fields Installed?' WHERE configuration_key = 'AMAZON_IL_ATTRIBUTES' LIMIT 1;