<?php
/**
 * amazon_il.php
 *
 * @package Amazon Inventory Loader
 * @copyright Copyright 2007-2008 Numinix Technology http://www.numinix.com
 * @copyright Portions Copyright 2003-2006 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: amazon_il.php, v0.99 02.15.2008 13:05:59 numinix $
 */
 
$define = [
    'TEXT_AMAZON_IL_STARTED' => 'Amazon Inventory Loader v.' . AMAZON_IL_VERSION . ' started ' . date("Y/m/d H:i:s"),
    'TEXT_AMAZON_IL_FILE_LOCATION' => 'Feed file - ',
    'TEXT_AMAZON_IL_FEED_COMPLETE' => 'Amazon Inventory Loader File Complete',
    'TEXT_AMAZON_IL_FEED_TIMER' => 'Time:',
    'TEXT_AMAZON_IL_FEED_SECONDS' => 'Seconds',
    'TEXT_AMAZON_IL_FEED_RECORDS' => ' Records',
    'AMAZON_IL_TIME_TAKEN' => 'In',
    'AMAZON_IL_VIEW_FILE' => 'View File:',
    'ERROR_AMAZON_IL_DIRECTORY_NOT_WRITEABLE' => 'Your Amazon Inventory Loader folder is not writeable! Please chmod the /' . AMAZON_IL_DIRECTORY . ' folder to 755 or 777 depending on your host.',
    'ERROR_AMAZON_IL_DIRECTORY_DOES_NOT_EXIST' => 'Your Amazon Inventory Loader output directory does not exist! Please create an /' . AMAZON_IL_DIRECTORY . ' directory and chmod to 755 or 777 depending on your host.',
    'ERROR_AMAZON_IL_OPEN_FILE' => 'Error opening Amazon Inventory Loader output file "' . DIR_FS_CATALOG . AMAZON_IL_DIRECTORY . AMAZON_IL_OUTPUT_FILENAME . '"',
    'TEXT_AMAZON_IL_ERRSETUP' => 'Amazon Inventory Loader error setup:',
    'TEXT_AMAZON_IL_ERRSETUP_L' => 'Amazon Inventory Loader Feed Language "%s" not defined in zen-cart store.',
    'TEXT_AMAZON_IL_ERRSETUP_C' => 'Amazon Inventory Loader Default Currency "%s" not defined in zen-cart store.'
];

$zc158 = (PROJECT_VERSION_MAJOR > 1 || (PROJECT_VERSION_MAJOR == 1 && substr(PROJECT_VERSION_MINOR, 0, 3) >= '5.8'));
if ($zc158) {
    return $define;
} else {
    nmx_create_defines($define);
}
//eof